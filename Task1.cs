using System;

namespace EPAM_NET_Lab
{
    /// <summary>
    /// Class Vector represents vectors that are set in three dimensions 
    /// </summary>
    class Vector
    {
        double _xCoord;
        double _yCoord;
        double _zCoord;

        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <param name="z">Z coordinate</param>
        public Vector(double x, double y, double z)
        {
            _xCoord = x;
            _yCoord = y;
            _zCoord = z;
        }

        /// <summary>
        /// Gets or sets X coordinate of the vector 
        /// </summary>
        public double CoordinateX
        {
            get
            {
                return _xCoord;
            }
            set
            {
                _xCoord = value;
            }
        }

        /// <summary>
        /// Gets or sets Y coordinate of the vector 
        /// </summary>
        public double CoordinateY
        {
            get
            {
                return _yCoord;
            }
            set
            {
                _yCoord = value;
            }
        }

        /// <summary>
        /// Gets or sets Z coordinate of the vector 
        /// </summary>
        public double CoordinateZ
        {
            get
            {
                return _zCoord;
            }
            set
            {
                _zCoord = value;
            }
        }

        /// <summary>
        /// Gets vector length 
        /// </summary>
        public double GetLength
        {
            get
            {
                return System.Math.Sqrt(_xCoord * _xCoord + _yCoord * _yCoord + _zCoord * _zCoord);
            }
        }

        /// <summary>
        /// Determines the equality of two vectors
        /// </summary>
        public static bool operator ==(Vector a, Vector b)
        {
            if ((object)a == (object)b)
            {
                return true;
            }
            if ((object)a == null || (object)b == null)
            {
                return false;
            }
            return a.Equals(b);
        }

        /// <summary>
        /// Determines the inequality of two vectors
        /// </summary>
        public static bool operator !=(Vector a, Vector b)
        {
            if ((object)a == (object)b)
            {
                return false;
            }
            if ((object)a == null || (object)b == null)
            {
                return false;
            }
            return !a.Equals(b);
        }

        /// <summary>
        /// Performs vector addition
        /// </summary>
        public static Vector operator +(Vector a, Vector b)
        {
            if ((object)a == null || (object)b == null)
            {
                throw new NullReferenceException("One or both operands are equal to null");
            }
            return new Vector(a.CoordinateX + b.CoordinateX, a.CoordinateY + b.CoordinateY, a.CoordinateZ + b.CoordinateZ);
        }

        /// <summary>
        /// Performs vector subtraction 
        /// </summary>
        /// </summary>
        public static Vector operator -(Vector a, Vector b)
        {
            if ((object)a == null || (object)b == null)
            {
                throw new NullReferenceException("One or both operands are equal to null");
            }
            return new Vector(a.CoordinateX - b.CoordinateX, a.CoordinateY - b.CoordinateY, a.CoordinateZ - b.CoordinateZ);
        }

        /// <summary>
        /// Calculates the product of a vector and a constant
        /// </summary>
        public static Vector operator *(Vector a, double constant)
        {
            if ((object)a == null)
            {
                throw new NullReferenceException("The operand is equal to null");
            }
            return new Vector(a.CoordinateX * constant, a.CoordinateY * constant, a.CoordinateZ * constant);
        }

        /// <summary>
        /// Static method calculates the angle between two vectors 
        /// </summary>
        public static double Angle(Vector a, Vector b)
        {
            if ((object)a == null || (object)b == null)
            {
                throw new NullReferenceException("One or both operands are equal to null");
            }
            return System.Math.Acos(Vector.DotProduct(a, b) / (a.GetLength * b.GetLength));
        }

        /// <summary>
        /// Static method calculates the dot product of two vectors 
        /// </summary>
        public static double DotProduct(Vector a, Vector b)
        {
            if ((object)a == null || (object)b == null)
            {
                throw new NullReferenceException("One or both operands are equal to null");
            }
            return a.CoordinateX * b.CoordinateX + a.CoordinateY * b.CoordinateY + a.CoordinateZ * b.CoordinateZ;
        }

        /// <summary>
        /// Static method calculates the cross product of two vectors 
        /// </summary>
        public static Vector CrossProduct(Vector a, Vector b)
        {
            if ((object)a == null || (object)b == null)
            {
                throw new NullReferenceException("One or both operands are equal to null");
            }
            double _newX = (a.CoordinateY * b.CoordinateZ) - (a.CoordinateZ * b.CoordinateY);
            double _newY = (a.CoordinateZ * b.CoordinateX) - (a.CoordinateX * b.CoordinateZ);
            double _newZ = (a.CoordinateX * b.CoordinateY) - (a.CoordinateY * b.CoordinateX);
            return new Vector(_newX, _newY, _newZ);
        }

        /// <summary>
        /// Static method calculates the triple product of three vectors 
        /// </summary>
        public static double TripleProduct(Vector a, Vector b, Vector c)
        {
            if ((object)a == null || (object)b == null || (object)c == null)
            {
                throw new NullReferenceException("One or both operands are equal to null");
            }
            return Vector.DotProduct(a, Vector.CrossProduct(b, c));
        }

        /// <summary>
        /// Returns a new Vector instance, which is the result of addition of vector a and vector b
        /// </summary>
        public static Vector Add(Vector a, Vector b)
        {
            return a + b;
        }

        /// <summary>
        /// Returns a new Vector instance, which is the result of subtraction of vector b from vector a
        /// </summary>
        public static Vector Subtract(Vector a, Vector b)
        {
            return a - b;
        }

        /// <summary>
        /// Returns a new Vector instance, which is the result of multiplication of each value of vector a by a constant
        /// </summary>
        public static Vector MultiplyBy(Vector a, double constant)
        {
            return a * constant;
        }

        /// <summary>
        /// Returns a new Vector instance, which is the result of addition of current and specified vectors
        /// </summary>
        public Vector Add(Vector a)
        {
            return this + a;
        }

        /// <summary>
        /// Returns a new Vector instance, which is the result of subtraction of current and specified vectors
        /// </summary>
        public Vector Subtract(Vector a)
        {
            return this - a;
        }

        /// <summary>
        /// Returns a new Vector instance, each value of which is multiplied by a constant
        /// </summary>
        public Vector MultiplyBy(double constant)
        {
            return this * constant;
        }

        /// <summary>
        /// Returns value that shows wether current Vector instance is equal to specified one
        /// </summary>
        public override bool Equals(object obj)
        {
            Vector other = obj as Vector;
            if (other == null)
            {
                return false;
            }
            return (this.CoordinateX == other.CoordinateX) && (this.CoordinateY == other.CoordinateY)
            && (this.CoordinateZ == other.CoordinateZ);
        }

        /// <summary>
        /// Returns hash code for current instance 
        /// </summary>
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        /// <summary>
        /// Returns string representation of current Vector instance 
        /// </summary>
        public override string ToString()
        {
            return string.Format("Vector: x = {0}, y = {1}, z = {2}", _xCoord, _yCoord, _zCoord);
        }
    }

    class Program
    {
        /// <summary>
        /// Static method that demonstrates the usage of Vector type
        /// </summary>
        public static void Main()
        {
            Vector a = new Vector(2, 4, 2.5);
            Vector b = new Vector(6, 7.4, 2.3);
            Vector c = new Vector(2, 4, 2.5);

            Console.WriteLine("\t\t\t\tEPAM .NET Lab");
            Console.WriteLine("\t\t\t\t    Task 1");
            Console.WriteLine("\tClass Vector represents vectors that are set in three dimensions");

            Console.WriteLine("Vector a:");
            Console.WriteLine("{0}\n", a);
            Console.WriteLine("Vector b:");
            Console.WriteLine("{0}\n", b);
            Console.WriteLine("Vector c:");
            Console.WriteLine("{0}\n", c);

            Console.WriteLine("a == b: \t{0}\n", a == b);
            Console.WriteLine("a == c: \t{0}\n", a == c);
            Console.WriteLine("a != c: \t{0}\n", a != c);
            Console.WriteLine("a + b: \t{0}\n", a + b);
            Console.WriteLine("a - b: \t{0}\n", a - b);
            Console.WriteLine("a * 5: \t{0}\n", a * 5);

            Console.WriteLine("Coordinates of c: \tx = {0}, y = {1}, z = {2}\n", c.CoordinateX, c.CoordinateY, c.CoordinateZ);
            Console.WriteLine("Length of b: \tl = {0:#####0.###}\n", b.GetLength);
            Console.WriteLine("Dot product of a and b: \ta .* b = {0}\n", Vector.DotProduct(a, b));
            Console.WriteLine("Cross product of a and b: \ta * b = {0}\n", Vector.CrossProduct(a, b));
            Console.WriteLine("Triple product of a, b and c: \ta .* (b * c) = {0}\n", Vector.TripleProduct(a, b, c));
            Console.WriteLine("Angle between vectors a and b: \talpha = {0:##0.###}\n", Vector.Angle(a, b));
        }
    }
}
