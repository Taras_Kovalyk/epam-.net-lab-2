using System;
using System.Text;
using System.Collections;

namespace EPAM_NET_Lab
{
    /// <summary>
    /// Class N_Vector represents n-dimensional vectors
    /// </summary>
    class N_Vector : IEnumerable
    {
        int[] _vectorArray;
        int _startIndex;
        int _endIndex;
        int _length;

        /// <param name="startIndex">Start index of n-vector</param>
        /// <param name="length">Length of n-vector</param>
        public N_Vector(int startIndex, int length)
        {
            _vectorArray = new int[length];
            _startIndex = startIndex;
            _length = length;
            _endIndex = _startIndex + length;
        }

        /// <param name="startIndex">Start index of n-vector</param>
        /// <param name="data">Array of integers containing values of n-vector</param>
        public N_Vector(int startIndex, int[] data)
        {
            _vectorArray = new int[data.Length];
            _startIndex = startIndex;
            _length = data.Length;
            _endIndex = _startIndex + _length;
            for (var i = 0; i < data.Length; i++)
            {
                _vectorArray[i] = data[i];
            }
        }

        /// <summary>
        /// Gets n-vector length 
        /// </summary>
        public int Length
        {
            get
            {
                return _length;
            }
        }

        /// <summary>
        /// Gets start index of the n-vector
        /// </summary>
        public int GetStartIndex
        {
            get
            {
                return _startIndex;
            }
        }

        /// <summary>
        /// Gets end index of the n-vector
        /// </summary>
        public int GetEndIndex
        {
            get
            {
                return _endIndex;
            }
        }

        /// <summary>
        /// Indexer that provides access to the elements of the n-vector 
        /// controlling the bounds
        /// </summary>
        public int this[int itemIndex]
        {
            get
            {
                if (itemIndex >= _startIndex || itemIndex < _endIndex)
                {
                    return _vectorArray[itemIndex - _startIndex];
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
            set
            {
                if (itemIndex >= _startIndex || itemIndex < _endIndex)
                {
                    _vectorArray[itemIndex - _startIndex] = value;
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
        }

        /// <summary>
        /// Determines the equality of two n-vector
        /// </summary>
        public static bool operator ==(N_Vector a, N_Vector b)
        {
            if ((object)a == null || (object)b == null)
            {
                return false;
            }
            if (a.GetStartIndex == b.GetStartIndex && a.Length == b.Length)
            {
                return a.Equals(b);
            }
            else
            {
                throw new InvalidOperationException("Only vectors with equal start and end indexes can be compared");
            }
        }

        /// <summary>
        /// Determines the inequality of two n-vector
        /// </summary>
        public static bool operator !=(N_Vector a, N_Vector b)
        {
            if ((object)a == null || (object)b == null)
            {
                return false;
            }
            if (a.GetStartIndex == b.GetStartIndex && a.Length == b.Length)
            {
                return !a.Equals(b);
            }
            else
            {
                throw new InvalidOperationException("Only vectors with equal start and end indexes can be compared");
            }
        }

        /// <summary>
        /// Performs n-vector addition
        /// </summary>
        public static N_Vector operator +(N_Vector a, N_Vector b)
        {
            if (a.GetStartIndex == b.GetStartIndex && a.Length == b.Length)
            {
                N_Vector result = new N_Vector(a.GetStartIndex, a.Length);

                for (var i = a.GetStartIndex; i < a.GetEndIndex; i++)
                {
                    result[i] = a[i] + b[i];
                }
                return result;
            }
            else
            {
                throw new InvalidOperationException("Only vectors with equal start and end indexes can be added");
            }
        }

        /// <summary>
        /// Performs n-vector subtraction 
        /// </summary>
        public static N_Vector operator -(N_Vector a, N_Vector b)
        {
            if (a.GetStartIndex == b.GetStartIndex && a.Length == b.Length)
            {
                N_Vector result = new N_Vector(a.GetStartIndex, a.Length);

                for (var i = a.GetStartIndex; i < a.GetEndIndex; i++)
                {
                    result[i] = a[i] - b[i];
                }
                return result;
            }
            else
            {
                throw new InvalidOperationException("Only vectors with equal start and end indexes can be subtracted");
            }
        }

        /// <summary>
        /// Calculates the product of a n-vector and a constant 
        /// </summary>
        public static N_Vector operator *(N_Vector a, int constant)
        {
            N_Vector result = new N_Vector(a.GetStartIndex, a.Length);

            for (var i = a.GetStartIndex; i < a.GetEndIndex; i++)
            {
                result[i] = a[i] * constant;
            }
            return result;
        }

		/// <summary>
        /// Returns a new N_Vector instance, which is the result of addition of n-vector a and n-vector b
        /// </summary>
        public static N_Vector Add(N_Vector a, N_Vector b)
        {
            return a + b;
        }
		
		/// <summary>
        /// Returns a new N_Vector instance, which is the result of subtraction n-vector a and n-vector b
        /// </summary>
        public static N_Vector Subtract(N_Vector a, N_Vector b)
        {
            return a - b;
        }
		
		/// <summary>
        /// Returns a new N_Vector instance, which is the result of multiplication of each value of n-vector a by a constant
        /// </summary>
		public static N_Vector MultiplyBy(N_Vector a, int constant)
        {
            return a * constant;
        }
		
        /// <summary>
        /// Returns a new N_Vector instance, which is the result of addition of current and specified n-vectors
        /// </summary>
        public N_Vector Add(N_Vector a)
        {
            return this + a;
        }

        /// <summary>
        /// Returns a new N_Vector instance, which is the result of subtraction of current and specified n-vectors
        /// </summary>
        public N_Vector Subtract(N_Vector a)
        {
            return this - a;
        }

        /// <summary>
        /// Returns a new N_Vector instance, each value of which is multiplied by a constant
        /// </summary>
        public N_Vector MultiplyBy(int constant)
        {
            return this * constant;
        }

        /// <summary>
        /// Returns value that shows wether current N_Vector instance is equal to specified one
        /// </summary>
        public override bool Equals(object obj)
        {
            N_Vector a = obj as N_Vector;
            if (a == null)
            {
                return false;
            }
            if (this.GetStartIndex == a.GetStartIndex && this.Length == a.Length)
            {
                for (var i = _startIndex; i < _endIndex - 1; i++)
                {
                    if (this[i] != a[i])
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// Returns string representation of current N_Vector instance 
        /// </summary>
        public override string ToString()
        {
            StringBuilder result = new StringBuilder(3 * _length);
            result.Append(string.Format("{0}-Vector: ", _length));
            result.Append('{');
            for (var i = 0; i < _length; i++)
            {
                if (i == _length - 1)
                {
                    result.Append(_vectorArray[i]);
                    result.Append('}');
                }
                else
                {
                    result.Append(_vectorArray[i]);
                    result.Append(", ");
                }
            }
            return result.ToString();
        }

        /// <summary>
        /// Returns hash code for current instance 
        /// </summary>
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        /// <summary>
        /// Returns the object that implements IEnumerator interface
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }

        /// <summary>
        /// Returns an instance of the N_VectorEnumerator class
        /// </summary>
        public N_VectorEnumerator GetEnumerator()
        {
            return new N_VectorEnumerator(_vectorArray);
        }
    }

    /// <summary>
    /// Class N_VectorEnumerator represents enumerator of the N_Vector type
    /// </summary>
    class N_VectorEnumerator : IEnumerator
    {
        public int[] _vectorArray;

        int position = -1;

        public N_VectorEnumerator(int[] vector)
        {
            _vectorArray = vector;
        }

        /// <summary>
        /// Moves to the next position
        /// </summary>
        public bool MoveNext()
        {
            position++;
            return (position < _vectorArray.Length);
        }

        /// <summary>
        /// Sets position to start value (-1)
        /// </summary>
        public void Reset()
        {
            position = -1;
        }

        /// <summary>
        /// Returns element at current position as object
        /// </summary>
        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        /// <summary>
        /// Returns element at current position
        /// </summary>
        public int Current
        {
            get
            {
                return _vectorArray[position];
            }
        }
    }
    class Program
    {
        /// <summary>
        /// Static method that demonstrates the usage of the N_Vector type
        /// </summary>
        public static void Main()
        {
            N_Vector a = new N_Vector(10, new int[] { 1, 2, 3, 4, 5 });
            N_Vector b = new N_Vector(20, new int[] { 2, 4, 6, 8, 10 });
            N_Vector c = new N_Vector(10, new int[] { 1, 2, 3, 4, 5 });
            N_Vector d = new N_Vector(10, new int[] { 1, 2, 3, 4, 5, 6 });
			
            Console.WriteLine("\t\t\t\tEPAM .NET Lab");
            Console.WriteLine("\t\t\t\t    Task 1");
            Console.WriteLine("\t\tClass N_Vector represents n-dimensional vectors");
			
            Console.WriteLine("Vector a: \t{0}", a);
            Console.WriteLine("Vector b: \t{0}", b);
            Console.WriteLine("Vector c: \t{0}", c);
            Console.WriteLine("Vector d: \t{0}", d);
			
            Console.WriteLine("\na == c: \t{0}\n", a == c);
            Console.WriteLine("a + c: \t\t{0}\n", a + c);
            Console.WriteLine("a - c: \t\t{0}\n", a - c);
            Console.WriteLine("Multiply a by 5: {0}\n", a.MultiplyBy(5));
            Console.WriteLine("Element a[13] = {0}\n", a[13]);
			
            Console.WriteLine("Enumerating each element of n-vector a using foreach construction: ");
            foreach (var element in a)
            {
                Console.Write("{0} ", element);
            }
			
            Console.WriteLine();
           
		    try
            {
                Console.WriteLine("\na == b:");
                Console.WriteLine(a == b);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            try
            {
                Console.WriteLine("\na + b:");
                Console.WriteLine(a + b);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            try
            {
                Console.WriteLine("\na - b:");
                Console.WriteLine(a - b);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}