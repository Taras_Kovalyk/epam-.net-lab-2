using System;
using System.Text;
using System.Collections;

namespace EPAM_NET_Lab
{
    /// <summary>
    /// Class Matrix represents rectangular matrix of size M*N
    /// </summary>
    class Matrix : IEnumerable
    {
        double[,] _matrix;
        int _m;
        int _n;
        int _length;

        public Matrix(int m, int n)
        {
            _matrix = new double[m, n];
            _m = m;
            _n = n;
            _length = _m * _n;
        }

        /// <param name="data">Two dimensional array containing values of a matrix</param>
        public Matrix(double[,] data)
        {
            _matrix = new double[data.GetLength(0), data.GetLength(1)];
            Array.Copy(data, _matrix, data.Length);
            _m = data.GetLength(0);
            _n = data.GetLength(1);
            _length = _m * _n;
        }

        /// <summary>
        /// Gets M-size of the matrix
        /// </summary>
        public int DimensionM
        {
            get
            {
                return _m;
            }
        }

        /// <summary>
        /// Gets N-size of the matrix
        /// </summary>
        public int DimensionN
        {
            get
            {
                return _n;
            }
        }

        /// <summary>
        /// Gets total number of elements in the matrix
        /// </summary>
        public int Length
        {
            get
            {
                return _matrix.Length;
            }
        }

        /// <summary>
        /// Indexer that provides access to the elements of the matrix 
        /// controlling the bounds
        /// </summary>
        public double this[int m, int n]
        {
            get
            {
                if (m >= 0 && m <= _matrix.GetLength(0) && n >= 0 && n <= _matrix.GetLength(1))
                {
                    return _matrix[m, n];
                }
                throw new IndexOutOfRangeException();
            }

            set
            {
                if (m >= 0 && m <= _matrix.GetLength(0) && n >= 0 && n <= _matrix.GetLength(1))
                {
                    _matrix[m, n] = value;
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
        }

        /// <summary>
        /// Determines the equality of two matrices
        /// </summary>
        public static bool operator ==(Matrix A, Matrix B)
        {
            return A.Equals(B);
        }

        /// <summary>
        /// Determines the equality of two matrices
        /// </summary>
        public static bool operator !=(Matrix A, Matrix B)
        {
            return !A.Equals(B);
        }

        /// <summary>
        /// Adds two matrices with the same sizes
        /// </summary>
        public static Matrix operator +(Matrix A, Matrix B)
        {
            if ((object)A == null || (object)B == null)
            {
                throw new NullReferenceException("Error: operands equal null");
            }
            if (A.DimensionM != B.DimensionM || A.DimensionN != B.DimensionN)
            {
                throw new InvalidOperationException("Matrix dimensions must agree");
            }

            Matrix result = new Matrix(A.DimensionM, A.DimensionN);

            for (var i = 0; i < A.DimensionM; i++)
            {
                for (var j = 0; j < A.DimensionN; j++)
                {
                    result[i, j] = A[i, j] + B[i, j];
                }
            }
            return result;
        }

        /// <summary>
        /// Subtracts one matrix from another. Matrices must be of the same sizes
        /// </summary>
        public static Matrix operator -(Matrix A, Matrix B)
        {
            if ((object)A == null || (object)B == null)
            {
                throw new NullReferenceException("Error: operands equal null");
            }
            if (A.DimensionM != B.DimensionM || A.DimensionN != B.DimensionN)
            {
                throw new InvalidOperationException("Matrix dimensions must agree");
            }

            Matrix result = new Matrix(A.DimensionM, A.DimensionN);

            for (var i = 0; i < A.DimensionM; i++)
            {
                for (var j = 0; j < A.DimensionN; j++)
                {
                    result[i, j] = A[i, j] - B[i, j];
                }
            }
            return result;
        }

        /// <summary>
        /// Calculates the product of a matrix and a constant
        /// </summary>
        public static Matrix operator *(Matrix A, double coef)
        {
            if ((object)A == null)
            {
                throw new NullReferenceException("Error: operands equal null");
            }

            Matrix result = new Matrix(A.DimensionM, A.DimensionN);

            for (var i = 0; i < A.DimensionM; i++)
            {
                for (var j = 0; j < A.DimensionN; j++)
                {
                    result[i, j] = A[i, j] * coef;
                }
            }
            return result;
        }

        /// <summary>
        /// Calculates the product of two matrices. 
        /// N-size of the first matrix must be equalk to M-size of the second matrix
        /// </summary>
        public static Matrix operator *(Matrix A, Matrix B)
        {
            if ((object)A == null || (object)B == null)
            {
                throw new NullReferenceException("Error: operands equal null");
            }
            if (A.DimensionN != B.DimensionM)
            {
                throw new InvalidOperationException("Matrix dimensions must agree");
            }

            Matrix result = new Matrix(A.DimensionM, B.DimensionN);
            double sum = 0;

            for (var i = 0; i < A.DimensionM; i++)
            {


                for (var k = 0; k < B.DimensionN; k++)
                {
                    sum = 0;

                    for (var j = 0; j < A.DimensionN; j++)
                    {
                        sum += A[i, j] * B[j, k];
                    }
                    result[i, k] = sum;
                }
            }
            return result;

        }

        /// <summary>
        /// Returns a new Matrix instance, which is the result of addition of current and specified matrices
        /// </summary>
        public Matrix Add(Matrix a)
        {
            return this + a;
        }

        /// <summary>
        /// Returns a new Matrix instance, which is the result of subtraction of specified matrix from current
        /// </summary>
        public Matrix Subtract(Matrix a)
        {
            return this + a;
        }

        /// <summary>
        /// Returns a new Matrix instance, which is multiplied by a specified matrix
        /// </summary>
        public Matrix Multiply(Matrix a)
        {
            return this * a;
        }

        /// <summary>
        /// Returns a new Matrix instance, each value of which is multiplied by a constant
        /// </summary>
        public Matrix Multiply(double constant)
        {
            return this * constant;
        }

        /// <summary>
        /// Returns transposed matrix
        /// </summary>
        public Matrix Transpose()
        {
            Matrix result = new Matrix(this.DimensionN, this.DimensionM);

            for (var i = 0; i < this.DimensionN; i++)
            {
                for (var j = 0; j < this.DimensionM; j++)
                {
                    result[i, j] = this[j, i];
                }
            }
            return result;
        }

        /// <summary>
        /// Returns a submatrix of current matrix
        /// </summary>
        /// <param name="mStart">The start value of m, which  the sample from matrix A begins with</param>
        /// <param name="mEnd">The end value of m, which  the sample from matrix A begins with</param>
        /// <param name="nStart">The start value of n, which  the sample from matrix A begins with</param>
        /// <param name="nEnd">The end value of n, which  the sample from matrix A begins with</param>
        public Matrix Submatrix(int mStart, int mEnd, int nStart, int nEnd)
        {
            if (mStart > this.DimensionM || mStart < 0 || nStart > this.DimensionN || nStart < 0
            || mEnd > this.DimensionM || mEnd < 0 || nEnd > this.DimensionN || nEnd < 0
            || mStart > mEnd || nStart > nEnd)
            {
                throw new InvalidOperationException("Error: incorrect size of submatrix");
            }

            Matrix result = new Matrix(mEnd - mStart + 1, nEnd - nStart + 1);

            for (var i = mStart; i <= mEnd; i++)
            {
                for (var j = nStart; j <= nEnd; j++)
                {
                    result[i - mStart, j - nStart] = this[i, j];
                }
            }
            return result;
        }

        /// <summary>
        /// Returns value that shows wether current Matrix instance is equal to a specified one
        /// </summary>
        public override bool Equals(object obj)
        {
            Matrix M = obj as Matrix;
            if (M == null)
            {
                return false;
            }
            if (M.DimensionM != this.DimensionM || M.DimensionN != this.DimensionN)
            {
                return false;
            }
            for (var i = 0; i < _m; i++)
            {
                for (var j = 0; j < _n; j++)
                {
                    if (_matrix[i, j] != M[i, j])
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Returns hash code for current instance 
        /// </summary>
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        /// <summary>
        /// Returns string representation of current Matrix instance 
        /// </summary>
        public override string ToString()
        {
            StringBuilder result = new StringBuilder(_matrix.Length * 3);
            result.AppendLine(string.Format("Matrix [{0} * {1}]: ", _m, _n));
            result.AppendLine();

            for (var i = 0; i < _matrix.GetLength(0); i++)
            {
                for (var j = 0; j < _matrix.GetLength(1); j++)
                {
                    result.Append(string.Format("{0} ", _matrix[i, j]).PadRight(8));
                }
                result.AppendLine();
                result.AppendLine();
            }
            return result.ToString();
        }

        /// <summary>
        /// Returns the object that implements IEnumerator interface
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }

        /// <summary>
        /// Returns an instance of the MatrixEnumerator class
        /// </summary>
        public MatrixEnumerator GetEnumerator()
        {
            return new MatrixEnumerator(_matrix);
        }
    }

    /// <summary>
    /// Class MatrixEnumerator represents enumerator of the Matrix type
    /// </summary
    class MatrixEnumerator : IEnumerator
    {
        public double[] _matrix;

        int position = -1;

        public MatrixEnumerator(double[,] matrix)
        {
            _matrix = new double[matrix.Length];
            for (var i = 0; i < matrix.GetLength(0); i++)
            {
                for (var j = 0; j < matrix.GetLength(1); j++)
                {
                    _matrix[j + i * matrix.GetLength(1)] = matrix[i, j];
                }
            }
        }

        /// <summary>
        /// Moves to the next position
        /// </summary>
        public bool MoveNext()
        {
            position++;
            return (position < _matrix.Length);
        }

        /// <summary>
        /// Sets position to start value (-1)
        /// </summary>
        public void Reset()
        {
            position = -1;
        }

        /// <summary>
        /// Returns element at current position as object
        /// </summary>
        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        /// <summary>
        /// Returns element at current position
        /// </summary>
        public double Current
        {
            get
            {
                return _matrix[position];
            }
        }
    }

    class Program
    {
        /// <summary>
        /// Static method that demonstrates the usage of the Matrix type
        /// </summary>
        public static void Main()
        {
            Matrix A = new Matrix(new double[,] { { 1, 3, 5, 7 }, { 3, 6, 5, 8 }, { 2, 8, 6, 7 } });
            Matrix B = new Matrix(new double[,] { { 1, 4 }, { 5, 5.6 }, { 2, 5.5 }, { 1.2, 1.5 } });
            Matrix C = new Matrix(new double[,] { { 1, 2, 3, 5 }, { 4, 5, 6, 6 }, { 7, 8, 9, 0 } });

            Console.WriteLine("\t\t\t\tEPAM .NET Lab");
            Console.WriteLine("\t\t\t\t    Task 4");
            Console.WriteLine("\tClass Matrix represents rectangular matrix of size M * N");

            Console.WriteLine("Matrix A: ");
            Console.WriteLine(A);
            Console.WriteLine("Matrix B: ");
            Console.WriteLine(B);
            Console.WriteLine("Matrix C: ");
            Console.WriteLine(C);

            Console.WriteLine("A + C: \n{0}", A + C);
            Console.WriteLine("A * 5: \n{0}", A * 5);
            Console.WriteLine("A * B: \n{0}", A * B);

            Console.WriteLine("Transposition of A: \n{0}", A.Transpose());
            Console.WriteLine("Submatrix m = 0..1, n = 0..1 of matrix C");
            Console.WriteLine(C.Submatrix(0, 1, 0, 1));
            Console.WriteLine("Element B[0, 1] = {0}", B[0, 1]);

            Console.WriteLine("\nEnumerating elements of the matrix A using foreach construction:");
            Console.Write("A = {");
            foreach (var element in A)
            {
                Console.Write("{0} ", element);

            }
            Console.Write("}");

            try
            {
                Console.WriteLine("\n\nA + B:");
                Console.WriteLine(A + B);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}